package com.antropix.tambouille.order.services.endpoint;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

import com.antropix.tambouille.order.domain.CancelOrderRequest;
import com.antropix.tambouille.order.domain.CancelOrderResponse;
import com.antropix.tambouille.order.domain.ObjectFactory;
import com.antropix.tambouille.order.domain.PlaceOrderRequest;
import com.antropix.tambouille.order.domain.PlaceOrderResponse;
import com.antropix.tambouille.order.services.OrderService;


/**
* <pre>
* This is the endpoint for the {@link OrderService}.
* Requests are simply delegated to the {@link OrderService} for processing.
* Two operations are mapped, using annotation, as specified in the link,
* <a href="http://static.springsource.org/spring-ws/sites/1.5/reference/html/server.html#server-at-endpoint"
* >http://static.springsource.org/spring-ws/sites/1.5/reference/html/server.html#server-at-endpoint</a
* ><ul>
*     <li>placeOrderRequest</li>
*     <li>cancelOrderRequest</li>
* </ul>
* </pre>
*
*/
@Endpoint
public class OrderServicePayloadRootAnnotationEndPoint {

    private final OrderService orderService;
    private final ObjectFactory JAXB_OBJECT_FACTORY = new ObjectFactory();
   
    public OrderServicePayloadRootAnnotationEndPoint(OrderService orderService) {
        this.orderService = orderService;
    }

    @PayloadRoot(localPart = "placeOrderRequest", namespace = "http://www.antropix.com/OrderService/schema")
    public JAXBElement<PlaceOrderResponse> getOrder(
            PlaceOrderRequest placeOrderRequest) {
        PlaceOrderResponse response = JAXB_OBJECT_FACTORY
                .createPlaceOrderResponse();
        response.setRefNumber(orderService.placeOrder(placeOrderRequest
                .getOrder()));

        return new JAXBElement<PlaceOrderResponse>(new QName(
                "http://www.antropix.com/OrderService/schema",
                "placeOrderResponse"), PlaceOrderResponse.class, response);
    }

    @PayloadRoot(localPart = "cancelOrderRequest", namespace = "http://www.antropix.com/OrderService/schema")
    public JAXBElement<CancelOrderResponse> cancelOrder(
            CancelOrderRequest cancelOrderRequest) {
        CancelOrderResponse response = JAXB_OBJECT_FACTORY
                .createCancelOrderResponse();
        response.setCancelled(orderService.cancelOrder(cancelOrderRequest
                .getRefNumber()));
        return new JAXBElement<CancelOrderResponse>(new QName(
                "http://www.antropix.com/OrderService/schema",
                "cancelOrderResponse"), CancelOrderResponse.class, response);
    }

}
